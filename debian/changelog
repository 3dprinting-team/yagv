yagv (0.4~20171211.r234ef16+dfsg-2) unstable; urgency=medium

  [ Debian Janitor ]
  * [3a9997b] Use secure copyright file specification URI.
  * [a6e7a4a] Bump debhelper from deprecated 9 to 12.
  * [790d790] Set debhelper-compat version in Build-Depends.
  * [c186525] Set upstream metadata fields: Bug-Database, Bug-Submit,
    Repository, Repository-Browse.

 -- Chow Loong Jin <hyperair@debian.org>  Sat, 28 Nov 2020 17:27:00 +0800

yagv (0.4~20171211.r234ef16+dfsg-1) unstable; urgency=medium

  * [6a6d2c8] New upstream version 0.4~20171211.r234ef16+dfsg
  * [bbc83bd] Port to python3
  * [a4bffc6] Update packaging to use python3
  * [9c40609] Drop python3-future dependency
    https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=878011 is fixed, so we
    don't need this any longer.
  * [ec67120] Drop 1000-typo-g1-g0.patch (upstreamed)
  * [95aae7a] Update vcs-* URLs

 -- Chow Loong Jin <hyperair@debian.org>  Wed, 25 Dec 2019 01:36:28 +0800

yagv (0.4~20130422.r5bd15ed+dfsg-4) unstable; urgency=medium

  * Updated obsolete link in Vcs-Git.
  * Rewrote handling of sharedmimeinfo to use dh_installmime.
  * Replaced text/x-gcode with text/x.gcode to be consistent with
    shared-mime-info.
  * Added python-future as a dependency as a workaround for bug
    #878011 in python-pyglet.

 -- Petter Reinholdtsen <pere@debian.org>  Thu, 12 Oct 2017 08:48:18 +0200

yagv (0.4~20130422.r5bd15ed+dfsg-3) unstable; urgency=medium

  * Added shared G-Code mime data and desktop file for improved GUI
    user experience.

 -- Petter Reinholdtsen <pere@debian.org>  Sun, 01 Oct 2017 20:23:18 +0200

yagv (0.4~20130422.r5bd15ed+dfsg-2) unstable; urgency=medium

  * Set Debian 3-D Printing team as maintainer.
  * Added gbp.conf to enforce the use of pristine-tar.
  * Corrected typo in package description (funciton->function).
  * Added dh-python as build dependency to keep dh_python2 happy.
  * Added d/patches/README to document patch numbering schema.
  * Renamed 2000-don-t-open-default-G-code-file.patch to follow new
    numbering scheme.
  * Added 1000-typo-g1-g0.patch to fix typo breaking parsing of Cura
    gcode files.  (Closes: #875313)
  * Changed Standards-Version from 3.9.5 to 4.1.1.
  * Added Vcs-Browser field to d/control.
  * Add myself as uploader.

 -- Petter Reinholdtsen <pere@debian.org>  Sun, 01 Oct 2017 16:44:39 +0200

yagv (0.4~20130422.r5bd15ed+dfsg-1) unstable; urgency=low

  * Initial release (Closes: #755273)

 -- Chow Loong Jin <hyperair@debian.org>  Sun, 20 Jul 2014 03:14:16 +0800
